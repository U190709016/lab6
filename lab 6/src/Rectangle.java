public class Rectangle {
    int sideA;
    int sideB;
    Point topLeft;
    public Rectangle(int sideA,int sideB,Point topLeft){
        this.sideA=sideA;
        this.sideB=sideB;
        this.topLeft=topLeft;
    }
    public double perimeter(){
        return (2*sideA + 2*sideB);

    }
    public double area(){
        return(sideA*sideB);

    }
    public String corners(){
        return ("| "+(topLeft.xCoord+" , " +topLeft.yCoord) + " | "+ ((topLeft.xCoord+sideA)+" , "+ topLeft.yCoord)
                +" | "+(topLeft.xCoord+sideA+" , "+(topLeft.yCoord+sideB))+" | "+(topLeft.xCoord+" , "+(topLeft.yCoord+sideB))+" |");

    }
}
