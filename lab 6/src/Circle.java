public class Circle {
    int radius;
    Point center;

    public double area(){
        return (Math.PI*(radius*radius));
    }
    public double perimeter(){
        return (2*Math.PI*radius);
    }
    public boolean intersect(Circle x){
        if(Math.sqrt(((center.xCoord - x.center.xCoord)*(center.xCoord-x.center.xCoord))+((center.yCoord-x.center.yCoord)*(center.yCoord - x.center.yCoord)))
        <=(radius+x.radius)){
            return true;
        }
        else{
            return false;
        }

    }




}
